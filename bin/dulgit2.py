import os
import sys
# APPENDS NEW DIRECTORIES WHICH PYTHON WILL SEARCH FOR THE MODULES TO IMPORT
dir = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'octamis_git_tools', 'bin','lib')
if not dir in sys.path:
    sys.path.append(dir)
from dulwich.repo import Repo
from dulwich.index import build_index_from_tree
from dulwich.refs import RefsContainer
from dulwich.objects import Tree
from dulwich.objects import Blob
from dulwich.objects import Commit
from dulwich.porcelain import ls_tree
from dulwich.porcelain import update_head
from dulwich.porcelain import clone
from dulwich.porcelain import push
from os import listdir
from os.path import isfile, join
import re

import logging




repo_path = '/home/tiago/Desktop/DumpGT/'
repo = Repo(repo_path)
index=repo.open_index()
obj_sto = repo.object_store


def show_commits(rw_dir):
	repo = Repo(rw_dir)
	commits=[]
	iterator=repo.head()
	commits.append([iterator,repo.get_object(iterator).message,repo.get_object(iterator).author])
	iterator=repo.get_parents(iterator)
	flag=True
	while flag==True:
		try:
			commits.append([iterator[0],repo.get_object(iterator[0]).message,repo.get_object(iterator[0]).author])
			iterator=repo.get_parents(iterator[0])
		except:
			flag = False
			pass
	return commits

def show_head(rw_dir):
	repo = Repo(rw_dir)
	head=repo.get_parents(repo.head())
	return head


"""from dulwich.patch import write_tree_diff
write_tree_diff(sys.stdout, repo.object_store, repo['402b78a212ae11b0a9e88787e83e52b70dcaf167'].tree, repo['530acc6f39f7a79d34a403ed014aa71e9118207f'].tree)"""
import shutil

def repo_clone(rw_dir):
	logger = logging.getLogger('gt')
	temp_dir=os.path.join(rw_dir,"temp")
	logger.info(rw_dir)
	logger.info(temp_dir)
	if os.path.exists(temp_dir):
		shutil.rmtree(temp_dir)
		print("The temporary directory has been purged")
	logger.info("Trying to clone")
	clone(rw_dir,temp_dir, bare=False, checkout=None, errstream=sys.stdout, outstream=None, origin='origin')
	"""except:
		print("no permission to clone repo into another repo")"""

def checkout(repo_path,commit):
	# CHANGES THE WORKING DIRECTORY TO THE COMMIT CHOSEN
	repo = Repo(repo_path)
	indexfile = repo.index_path()
	print(indexfile)
	obj_sto = repo.object_store
	print(obj_sto)
	tree_id = repo[commit].tree
	print(tree_id)
	build_index_from_tree(repo_path,indexfile,obj_sto,tree_id)
	tree_entries = [obj_sto.iter_tree_contents(tree_id)]

def update_head(rw_dir,commit):
	
	repo = Repo(rw_dir)
	repo.refs[b'refs/heads/master'] = commit
	
	

def commit(rw_dir,message,author,email):
	print("I'm commiting")
	identification=author + " " + "<" + email + ">" +"\""
	repo = Repo(rw_dir)
	repo.do_commit(message, committer=identification)
	print("Commited %s %s", message,identification)



def stage(rw_dir):
	gtfiles_path = os.path.join(rw_dir,'files')
	if os.path.exists(gtfiles_path)==False:
		os.mkdir(gtfiles_path)
	repo = Repo(rw_dir)
	repo_gt = [f.encode('utf-8') for f in listdir(rw_dir) if isfile(join(rw_dir, f))]
	repo_gtfiles = [f.encode('utf-8') for f in listdir(gtfiles_path) if isfile(join(gtfiles_path, f))]
	for f in repo_gt:
		repo.stage([f])
	for f in repo_gtfiles:
		repo.stage([os.path.join('files',f)]) # THE FILES REFERRED FOR ADD ARE ALWAYS RELATIVE TO THE WORKING TREE PATH (SO THE gtfiles need to have the prefix "files" to be found)
	staged_files=[",".join([f.decode(sys.getfilesystemencoding()) for f in repo.open_index()])]
	print(staged_files)

def purge(rw_dir, pattern):
    for f in os.listdir(rw_dir):
        if os.path.isfile(os.path.join(rw_dir, f)) and not re.search(pattern, f):
			print(os.path.join(rw_dir, f))
			os.remove(os.path.join(rw_dir, f))
    for f in os.listdir(os.path.join(rw_dir,"files")):
		print(os.path.join(os.path.join(rw_dir,"files"), f))
		os.remove(os.path.join(os.path.join(rw_dir,"files"), f))




#clone('/home/tiago/Desktop/DumpGT','/home/tiago/Desktop/DumpGT/temp', bare=False, checkout=None, errstream=sys.stdout, outstream=None, origin='origin')
#push('/home/tiago/Desktop/DumpGT/', 'https://tiago@octamis.com:K9azp4QNZ73@bitbucket.org/octamis/test_glasstable_repo', 'master', outstream=sys.stdout, errstream=sys.stdout)


#https://bitbucket.org/octamis/test_glasstable_repo
repo_clone('/home/tiago/Desktop/DumpGT/')
#purge('/home/tiago/Desktop/DumpGT/temp',".git")
#checkout('/home/tiago/Desktop/DumpGT/temp','010ab4f838c4eb30cf0553023a538e88f53ba3e1')
#stage('/home/tiago/Desktop/DumpGT/temp')
#commit('/home/tiago/Desktop/DumpGT/temp/',"dummy","tiago","tiago@octamis.com")
#update_head('/home/tiago/Desktop/DumpGT/temp','010ab4f838c4eb30cf0553023a538e88f53ba3e1')
#checkout('/home/tiago/Desktop/DumpGT/temp','010ab4f838c4eb30cf0553023a538e88f53ba3e1')

sys.exit()
#
try:
	clone('/home/tiago/Desktop/DumpGT/', '/home/tiago/Desktop/DumpGT/GT3/', bare=False, checkout=None, errstream=sys.stdout, outstream=None, origin='origin')
except:
	print("no permission to clone repo into another repo")
	
sys.exit()
checkout('/home/tiago/Desktop/DumpGT/','6c23c20530a7a3f60189018eb2a3e62963af3403')
	




#print(show_commits('/home/tiago/Desktop/DumpGT/'))
#print(show_head('/home/tiago/Desktop/DumpGT/'))

update_head('/home/tiago/Desktop/DumpGT/','e37bc5297aebbd43ebaaefbbd4532707c7b7bfcf', detached=False, new_branch=None)


sys.exit()

tree_id = repo['367d3d58acd5f16b6bb94fad726b268a6f12f7b8'].tree
tree_entries = [obj_sto.iter_tree_contents(tree_id)]



indexfile = repo.index_path()
refs=repo.get_refs()
print(refs)

obj_sto = repo.object_store
tree_id = repo[co_ref].tree
print(tree_id)
build_index_from_tree(repo_path,indexfile,obj_sto,tree_id)
tree_entries = [obj_sto.iter_tree_contents(tree_id)]

#tree_entries.next()  
