#!/usr/bin/env python
# coding=utf-8
#
# Copyright © 2011-2015 Splunk, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"): you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


import os, stat
import sys
#import ast

dir = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'octamis_git_ucc', 'bin','lib')
if not dir in sys.path:
    sys.path.append(dir)

dir = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'octamis_git_ucc', 'lib')
if not dir in sys.path:
    sys.path.append(dir)


#import httplib, urllib
#import urllib2
#from urllib import quote
import six.moves.http_client, six.moves.urllib.request as urllib2
import six.moves.urllib.parse, six.moves.urllib.error as urllib_error
from six.moves.urllib.parse import quote
import requests
import time
import json
import re
import time
import random
import csv
from os import listdir
from os.path import isfile, join
from collections import OrderedDict
import shutil
import splunk.rest as rest

"""all the contained files in the .git must have rwx permission for splunk user (either by assigning him to a linux group, or chmod -R o+w *)
using splunk to stage files changes the owner to splunk and the permissions to 710 """

from dulwich.repo import Repo
from dulwich.porcelain import clone
from dulwich.index import build_index_from_tree
from dulwich.porcelain import push

# splunklib IS A MODULE THAT CONTAINS 
# Splunk related imports
from splunk.clilib import cli_common as cli 
from splunklib.searchcommands import dispatch, StreamingCommand, Configuration, Option, validators,EventingCommand
import logging 
import framework.log as log
import splunklib.client
import splunklib.results as results
from splunklib.client import connect
from splunk.clilib import cli_common as cli



import stat, sys, io
from builtins import bytes

from dulwich import porcelain
from dulwich.objects import Blob, Tree
from dulwich.repo import MemoryRepo
import csv

import base64
import hashlib
import boto3

from solnlib import credentials



@Configuration()
class SneakyGit(EventingCommand):


    action = Option(
    doc='''
        Syntax: action=<action>
        Description: Specify what action to do, check_auth head, checkout, push_remote''',
        require=True)
     
        
    branch = Option(
    doc='''
        Syntax: branch=<branch_name>
        Description: remote branch to push changes to''',
        require=False)

    file = Option(
    doc='''
        Syntax: file=<file>
        Description: file to be pushed to repo''',
        require=False)
        

    message = Option(
    doc='''
        Syntax: message=<message>
        Description: message of the commit''',
        require=False,default="commit_message")

    
    repo_url = Option(
    doc='''
        Syntax: repo_url=<url>, only the part after @
        Description: remote url to push changes in the format https://bitbucket.org/octamis/ta-breachops-ingredients-annotations.git''',
        require=True,default="dummy")

    force = Option(
    doc='''
        Syntax: force=<bool>
        Description: decide whether to force a push to non-synced version remotely (commit? push?)''',
        require=False,default="False")

    fallback_user_default = Option(
    doc='''
        Syntax: fallback_user_default=<bool>
        Description: if a user hasn't got credentials in the system, allows to fallback the push_remote as the designated default user ''',
        require=False,default="True")

    email = Option(
    doc='''
        Syntax: email=<mail>
        Description: Email of the commiter''',
        require=False,default="youremail@mail.com")

    write_mode = Option(
        doc='''
        Syntax: write_mode=<mode>
        Description: what to do with prior file contents: replace = discard, append = retain, appending new records, upsert = append if record does not exist, else update that record (")
        (*) Requires you specify upsert_key field to identify clashing records''',
        require=False, default="replace")

    upsert_pk = Option(
        doc='''
        Syntax: upsert_pk=<field>
        Only used it write_mode="upsert"
        The field name which (like a primary key) identifies a new incoming record as being the same subjectmatter as an existing one, triggering an update instead of an append''',
        require=False, default="mustSpecifyField")


    logger = logging.getLogger('gt')        
    cfg = cli.getConfStanza('octamis_git_ucc_settings',"logging")
    logger.setLevel(cfg['loglevel'])

    def show_commits(self,remote_repo,git_client,branch):

        self.logger.debug("Showing Commits")
        self.logger.debug(git_client,branch)
        local_repo = MemoryRepo()
        errIO = io.BytesIO()
        outIO = io.StringIO()
        local_repo.refs.set_symbolic_ref(b'HEAD',b'refs/heads/'+branch.encode('utf-8'))
        commits=[]
        if git_client == "codecommit":
            self.logger.debug("git_client is codecommit")
            remote_repo = "https://tmatos-at-318059165041:tNxQ4f44MASdRipejU1bwZWz4gO1jpYWmTzjIYK+Pf0=@git-codecommit.eu-west-2.amazonaws.com/v1/repos/TestRepo"

        try:
            porcelain.fetch(local_repo, remote_repo, errstream=errIO, outstream=outIO)
            self.logger.debug("After fetch in show commits")
            local_repo[b'refs/heads/'+branch.encode('utf-8')] = local_repo[b'refs/remotes/origin/'+branch.encode('utf-8')]
            iterator=local_repo.head()
            commits.append([iterator,local_repo.get_object(iterator).message,local_repo.get_object(iterator).author,local_repo.get_object(iterator).committer,time.strftime("%d/%m/%Y %H:%M:%S %Z",time.localtime(local_repo.get_object(iterator).author_time)),branch])
            iterator=local_repo.get_parents(iterator)
            flag=True
            while flag==True:
                try:
                    commits.append([iterator[0],local_repo.get_object(iterator[0]).message,local_repo.get_object(iterator[0]).author,local_repo.get_object(iterator[0]).committer,time.strftime("%d/%m/%Y %H:%M:%S %Z",time.localtime(local_repo.get_object(iterator[0]).author_time)),branch])
                    iterator=local_repo.get_parents(iterator[0])
                #except:
                except Exception as e:
                    flag = False
                    pass
        except Exception as e:
            self.logger.error(str(e))
            if str(e).find("401")!=-1:
                self.logger.debug("Most likely a token refresh needed")
            pass
        return commits


                
    def show_head(self,repo,branch):
        self.logger.debug("Inside show Head")
        head = ""
        try:
            repo[b'refs/heads/'+branch.encode('utf-8')] = repo[b'refs/remotes/origin/'+ branch.encode('utf-8')]
            iterator=repo.head()    
            head=[iterator,repo.get_object(iterator).message,repo.get_object(iterator).author,repo.get_object(iterator).committer,time.strftime("%d/%m/%Y %H:%M:%S %Z",time.localtime(repo.get_object(iterator).author_time))]
        except:
            self.logger.debug("There was no previous tree, this is an initial commit into repo %s branch %s", repo, branch)
            pass
        return head
       

    def refreshtoken(self,clientid,secret,refresh_token):

        self.logger.debug("Inside Refresh Token")
        self.logger.debug("refreshtoken: called with clientid=%s, secret=%s, refresh_token=%s" % (clientid, secret, refresh_token))
        mydata={'grant_type':'refresh_token','refresh_token':refresh_token}
        myurl='https://bitbucket.org/site/oauth2/access_token'
        self.logger.debug("refreshtoken: mydata=%s, myurl=%s" % (mydata, myurl))
        x = requests.post(myurl, data = mydata, auth=(clientid,secret))
        jresponse=json.loads(x.text)
        self.logger.debug("refreshtoken: jresponse=%s, access_token=%s, refresh_token=%s" % (jresponse, jresponse["access_token"], jresponse["refresh_token"]))

        return jresponse["access_token"], jresponse["refresh_token"]

    


    def postmessage(self,repo,branch,name,msg,severity):

        session_key = self._metadata.searchinfo.session_key
        splunkd_uri = self._metadata.searchinfo.splunkd_uri
        message_url=''.join([splunkd_uri,"/services/messages"])

        headers = {'Authorization': 'Splunk %s' % session_key,'Content-Type': 'application/json'}
        mess_text = { 'name' : name, 'value' :'Repo ' + repo + ' with branch ' + branch + ': ' + msg, 'severity': severity }
        resp = requests.post(message_url, mess_text , headers=headers, verify=False)
        self.logger.debug(resp)

        return



    def get_oauth(self):

        self.logger.debug("getting oauth")
        session_key=self._metadata.searchinfo.session_key
        c=credentials.CredentialManager(session_key,'octamis_git_ucc',realm='__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account')
        storage_passwords=self.service.storage_passwords
        remote_path=""
        clientid=""
        secret=""
        refresh_token=""
        git_user=""
        splunkuser=""

        for credential in storage_passwords:
            self.logger.debug("Looping storage passwords")
            #self.logger.debug(credential.content)
            #self.logger.debug(credential.content.username)
            splunkuser=credential.access()['owner']
            self.logger.debug("splunk credential owner is %s", splunkuser)
            self.logger.debug("splunk current user is %s", self._metadata.searchinfo.username)
            if splunkuser==self._metadata.searchinfo.username:
                account_stanza_name=re.search(r'(.*)``splunk_cred_sep``',credential.content.username)
                self.logger.debug("account_name is %s",account_stanza_name.group(1))
                account_stanza_name=account_stanza_name.group(1)
                full_creds=json.loads(c.get_password(account_stanza_name))
                cfg = cli.getConfStanza('octamis_git_ucc_account',account_stanza_name)
                # THIS MAY NEED MODIFICATION TO COPE WITH CODECOMMIT
                git_client_pattern=re.search(r'https://([^\.]*)\.',cfg['url'])
                git_client_pattern=git_client_pattern.group(1)
                auth_type="oauth"
                clientid=cfg['client_id']
                #self.logger.debug(full_creds)
                access_token=full_creds['access_token']
                refresh_token=full_creds['refresh_token']
                secret=full_creds['client_secret']
                git_user=account_stanza_name
                #self.logger.debug("access is %s",access_token)
                #self.logger.debug("refresh is %s",refresh_token)
                #self.logger.debug("secret is %s",secret)
                remote_path='https://x-token-auth:' + access_token + '@' + str(self.repo_url)
                break   
            else:
                self.logger.debug("This entry doesn't belong to the current user")

        if remote_path=="":
            self.logger.info("The current user has no associated git credential")

        return clientid, secret, refresh_token, git_user, splunkuser


    def identify_auth(self):
        self.logger.debug("In identify_auth")

        remote_path = ""
        git_user = ""
        auth_type = ""
        storage_passwords=self.service.storage_passwords

        refresh_token=""
        git_client_pattern = ""

        self.logger.debug(storage_passwords)

        session_key=self._metadata.searchinfo.session_key
        c=credentials.CredentialManager(session_key,'octamis_git_ucc',realm='__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account')
        #self.logger.debug(c.get_password(self._metadata.searchinfo.username))

        for credential in storage_passwords:
            splunkuser=credential.access()['owner']
            self.logger.debug("splunk credential owner is %s", splunkuser)
            self.logger.debug("splunk current user is %s", self._metadata.searchinfo.username)
            if splunkuser==self._metadata.searchinfo.username:
                account_stanza_name=re.search(r'(.*)``splunk_cred_sep``',credential.content.username)
                self.logger.debug("account_name is %s",account_stanza_name.group(1))
                account_stanza_name=account_stanza_name.group(1)
                full_creds=json.loads(c.get_password(account_stanza_name))
                pattern_basic_auth=re.search(r'\"access_token\"',credential.content.clear_password)
                cfg = cli.getConfStanza('octamis_git_ucc_account',account_stanza_name)
                # THIS MAY NEED MODIFICATION TO COPE WITH CODECOMMIT
                git_client_pattern=re.search(r'https://([^\.]*)\.',cfg['url'])
                git_client_pattern=git_client_pattern.group(1)
                auth_type="oauth"
                if pattern_basic_auth != None:
                    self.logger.debug("this user is using oauth")
                    auth_type="oauth"
                    access_token=full_creds['access_token']
                    refresh_token=full_creds['refresh_token']
                    #self.logger.debug("access is %s",access_token)
                    #self.logger.debug("refresh is %s",refresh_token)

                else:
                    auth_type="basic" 
                    self.logger.debug("this user is using basic auth")
                    password=full_creds['password']
                    git_account_name=cfg['username']
                break   
            else:
                self.logger.debug("This entry doesn't belong to the current user")
        if auth_type=="basic":
            remote_path='https://' + str(git_account_name) + ':' + str(password) + '@' + str(self.repo_url)
            #self.logger.debug(remote_path)
        elif auth_type=="oauth" and git_client_pattern=="bitbucket":
            remote_path='https://x-token-auth:' + str(access_token) + '@' + str(self.repo_url)
        elif auth_type=="oauth" and git_client_pattern=="codecommit":
            #self.logger.debug("AWS CODECOMMIT!!!")
            remote_path='url:' + self.repo_url + '||' + 'password:' + password + '||' + 'clientid:' + clientid + '||' + 'secret:' + secret

        if remote_path != "":
            self.logger.debug("remote path is well formed")
            #self.logger.debug(remote_path)
        else:
            self.logger.debug("There were no credentials for this user " + self._metadata.searchinfo.username)
            if self.fallback_user_default == "True":
                # DEFAULT USER CODE YET TO BE DONE FOR UCC-GEN
                self.logger.debug("Continuing the search, now looking for a DEFAULT USER")
                for credential in storage_passwords:
                    account_stanza_name=re.search(r'(.*)``splunk_cred_sep``',credential.content.username)
                    self.logger.debug("account_name is %s",account_stanza_name.group(1))
                    account_stanza_name=account_stanza_name.group(1)
                    cfg = cli.getConfStanza('octamis_git_ucc_account',account_stanza_name)
                    full_creds=json.loads(c.get_password(account_stanza_name))
                    default_user=cfg['default_user']
                    repo_url_git_client_pattern=re.search(r'^([^\.]*)\.',self.repo_url)
                    repo_url_git_client_pattern=repo_url_git_client_pattern.group(1)
                    self.logger.debug("The repo's git client is %s",repo_url_git_client_pattern)
                    self.logger.debug("default_user is %s",default_user)
                    git_client_pattern=re.search(r'https://([^\.]*)\.',cfg['url'])
                    #self.logger.debug(git_client_pattern.group(1))
                    git_client_pattern=git_client_pattern.group(1)
                    self.logger.debug("git_client is %s",git_client_pattern)
                    self.logger.debug("repo_url_git_client_pattern is %s",repo_url_git_client_pattern)
                    if default_user == "1" and git_client_pattern==repo_url_git_client_pattern and git_client_pattern=="bitbucket":
                        self.logger.debug("A default user was found for client %s",git_client_pattern)
                        pattern_basic_auth=re.search(r'\"access_token\"',credential.content.clear_password)
                        #self.logger.debug(pattern_basic_auth)
                        if pattern_basic_auth != None:
                            self.logger.debug("this user is using oauth")
                            auth_type="oauth"
                            access_token=full_creds['access_token']
                            refresh_token=full_creds['refresh_token']
                            #self.logger.debug("access is %s",access_token)
                            #self.logger.debug("refresh is %s",refresh_token)
                            remote_path='https://x-token-auth:' + access_token + '@' + str(self.repo_url)

                        elif pattern_basic_auth == None: 
                            auth_type="basic" 
                            self.logger.debug("this user is using basic auth")
                            password=full_creds['password']
                            #self.logger.debug("password is %s",password)
                            git_account_name=cfg['username']
                            remote_path='https://' + str(git_user) + ':' + str(password) + '@' + str(self.repo_url)
                        break
                    elif (default_user == "1" and git_client_pattern==repo_url_git_client_pattern and git_client_pattern=="codecommit"):
                            self.logger.debug("Codecommit branch is in development!!!")
                        
            else:
                self.logger.debug("Fallback to default user is False, so nothing else to be done")

        return remote_path, splunkuser, auth_type, git_client_pattern


    def get_default_user(self):

        storage_passwords=self.service.storage_passwords
        session_key=self._metadata.searchinfo.session_key
        c=credentials.CredentialManager(session_key,'octamis_git_ucc',realm='__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account')
        self.logger.debug("The current user has no associated git credential. Falling back to a default user")
        for credential in storage_passwords:
            account_stanza_name=re.search(r'(.*)``splunk_cred_sep``',credential.content.username)
            account_name = account_stanza_name.group(1)
            self.logger.debug(f"account_name is {account_name}")
            account_stanza_name=account_name
            cfg = cli.getConfStanza('octamis_git_ucc_account',account_stanza_name)
            full_creds=json.loads(c.get_password(account_stanza_name))
            default_user=cfg['default_user']
            repo_url_git_client_pattern=re.search(r'^([^\.]*)\.',self.repo_url)
            repo_url_git_client_pattern=repo_url_git_client_pattern.group(1)
            self.logger.debug("repo's git client is %s",repo_url_git_client_pattern)
            self.logger.debug("default_user is %s",default_user)
            git_client_pattern=re.search(r'https://([^\.]*)\.',cfg['url'])
            git_client_pattern=git_client_pattern.group(1)
            self.logger.debug("git_client is %s",git_client_pattern)
            self.logger.debug("repo_url_git_client_pattern is %s",repo_url_git_client_pattern)
            if default_user == "1" and git_client_pattern==repo_url_git_client_pattern and git_client_pattern=="bitbucket":
                self.logger.debug("Found a default user for client %s",git_client_pattern)
                pattern_basic_auth=re.search(r'\"access_token\"',credential.content.clear_password)
                #self.logger.debug(pattern_basic_auth)
                if pattern_basic_auth != None:
                    self.logger.debug("this is oauth")
                    auth_type="oauth"
                    access_token=full_creds['access_token']
                    refresh_token=full_creds['refresh_token']
                    secret=full_creds['client_secret']
                    clientid=cfg['client_id']
                    #self.logger.debug("access is %s",access_token)
                    #self.logger.debug("refresh is %s",refresh_token)
                    #self.logger.debug("secret is %s",secret)
                    #self.logger.debug("clientid is %s",clientid)
                    remote_path='https://x-token-auth:' + access_token + '@' + str(self.repo_url)
                elif pattern_basic_auth == None: 
                    auth_type="basic" 
                    self.logger.debug("this is basic auth")
                    password=full_creds['password']
                    #self.logger.debug("password is %s",password)
                    git_account_name=cfg['username']
                    remote_path='https://' + str(git_user) + ':' + str(password) + '@' + str(self.repo_url)
                break
            elif (default_user == "1" and git_client_pattern==repo_url_git_client_pattern and git_client_pattern=="codecommit"):
                    self.logger.debug("TO BE COMPLETED AWS CODECOMMIT!!!")

        return clientid, secret, refresh_token, account_name


    def store_refresh_token(self,access_token,git_user,secret,refresh_token):

        access_token=str(access_token)
        session_key=self._metadata.searchinfo.session_key
        c=credentials.CredentialManager(session_key,'octamis_git_ucc',realm='__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account')
        c.set_password(git_user,"{\"access_token\": \"" + access_token + "\", \"client_secret\": \"" + secret + "\", \"refresh_token\": \"" + refresh_token + "\"}") 
        self.logger.debug("Stored Password")  



    def gitfetch(self,remote_path,auth_type,git_client,git_user,branch):

        self.logger.debug("In GitFetch")
        local_repo = MemoryRepo()
        refs_heads_branch=bytes('refs/heads/'+ branch,'utf-8')
        local_repo.refs.set_symbolic_ref(b'HEAD',refs_heads_branch)

        # fetch into our local in memory repo from bitbucket
        errIO = io.BytesIO()
        outIO = io.StringIO()
    
        if git_client == "codecommit":
            pattern = re.compile('url:git-codecommit.eu-west-2.amazonaws.com/v1/repos/(.*)\|\|password:(.*)\|\|clientid:(.*)\|\|secret:(.*)')
            match = pattern.match(remote_path)
            aws_repo = match.group(1)
            password = match.group(2)
            clientid = match.group(3)
            secret = match.group(4)
            #self.logger.debug(aws_repo + password + clientid + secret)
            remote_path = "https://" + git_user + ":" + password + "@git-codecommit.eu-west-2.amazonaws.com/v1/repos/" + aws_repo
        #self.logger.debug(remote_path + git_user + auth_type)

        exception_remediated=False
        try:
            # BASIC AUTH FOR GIT is NOT RECOMMENDED. PASSWORD EXOTIC CHARACTERS BREAK THE LOGIC
            porcelain.fetch(local_repo, remote_path, errstream=errIO, outstream=outIO)
        except Exception as e:
            self.logger.error("exception: err output is %s", str(e))
            self.logger.error(str(e).find("401")!=-1 and auth_type=="basic")

            if str(e).find("401")!=-1 and auth_type=="basic":
                self.logger.error("Connection error: either password has been changed, expired, or connectivity to bitbucket is not working")
                e = str(e) + " Connection error: either password has been changed, expired, or connectivity to btibucket is not working"
                #yield {'_time': time.time(),'response': "Connection error: either the bitbucket password has been changed, expired, or connectivity to bitbucket is not working"}
            elif str(e).find("401")!=-1 and auth_type=="oauth" and git_client == "bitbucket" :
                self.logger.info("Looks like the token has expired and a refresh will be requested")
                e = str(e) + "Looks like the token has expired and a refresh will be requested"
                clientid, secret, refresh_token, git_user, splunkuser = self.get_oauth()
                #self.logger.debug(clientid  + "||" + secret + "||" + refresh_token)
                # If no crentials were collected, then there is no credential for the current user
                if refresh_token == "" and self.fallback_user_default == "True":
                    clientid,secret,refresh_token,account_name = self.get_default_user()
                    self.logger.debug(f"got default user {account_name}")
                    git_user=account_name
                        
                new_access_token,refresh_token=self.refreshtoken(clientid,secret,refresh_token)
                self.logger.debug("refreshtoken successful")
                #self.logger.debug(f"About to store refreshtoken new_access_token={new_access_token}, git_user={git_user}, secret={secret}, refresh_token={refresh_token}")
                self.store_refresh_token(new_access_token,git_user,secret,refresh_token)
                self.logger.debug("Stored Password successful")  
                self.logger.debug("gitfetch: new_access_token=%s, repo_url=%s" % (new_access_token or "?", str(self.repo_url) or "?"))
                remote_path='https://x-token-auth:' + new_access_token + '@' + str(self.repo_url)
                self.logger.debug("gitfetch: remote_path=%s" % (remote_path or "?"))
                try:
                    porcelain.fetch(local_repo, remote_path, errstream=errIO, outstream=outIO)
                except Exception as ee:
                    self.logger.debug("gitfetch: exception caught during fetch: %s" % (ee))
                else:
                    self.logger.debug("gitfetch: fetch operation succeeded")
                    exception_remediated=True
                finally:
                    self.logger.debug("gitfetch: fetch operation finally")
                    self.logger.debug("gitfetch: fetch operation finally: errIO="+errIO.getvalue().decode("utf-8"))
                    self.logger.debug("gitfetch: fetch operation finally: outIO="+outIO.getvalue())
                self.logger.debug("gitfetch: fetch done")
                pass
            else:
                self.logger.debug("Non 401 error - Fetch was not successful")

        try:
            exception = e
        except Exception as e:
            exception = ""

        return local_repo, exception, exception_remediated, remote_path


    def commit_crosshcheck(self,remote_head,repo_url,branch):

            session_key = self._metadata.searchinfo.session_key
            splunkd_uri = self._metadata.searchinfo.splunkd_uri

            service = splunklib.client.connect(token=session_key, app="octamis_git_ucc")
            collection_name = "git_versions"
            collection = service.kvstore[collection_name]
            kv_query={"repo": repo_url ,  "branch" : branch}
            self.logger.debug("the fields to match in git_versions are %s",kv_query)
            git_repo=collection.data.query(query=json.dumps(kv_query))
            self.logger.debug("the repo status in git_versions is %s",git_repo)

            repo_stanza_name="repo_input://"+self.repo_url
            self.logger.debug("The repo stanza to be queried from inputs.conf is %s", repo_stanza_name)
            cfg = cli.getConfStanza('inputs',repo_stanza_name)
            self.logger.debug("the full config is %s",cfg)
            if len(cfg) > 1 and cfg['branch'] == branch :
                self.logger.debug("There was a stanza with that repo name and that branch")
            else:
                self.logger.debug("There was no stanza with that repo name and or with specified branch")

            if len(git_repo) == 0:
                self.logger.debug("This is a deviating case where you're pushing to a repo you didn't input to the app!")
                
            else:
                entry_key=git_repo[0]["_key"]
                local_commit=git_repo[0]["clienthash"]
                local_branch=git_repo[0]["branch"]

                if isinstance(remote_head, (bytes, bytearray)):
                    self.logger.debug("remote_head was bytes")
                    remote_head = remote_head.decode('UTF-8')
                else:
                    self.logger.debug("value was NOT bytes")

                self.logger.debug("Remote hash is %s Local hash is %s Branch is %s",remote_head,local_commit,local_branch)

                if remote_head == local_commit:
                    res = "synchronized"
                else:
                    res = "desynchronized"

                return res


    def get_branch(self,repo_url):

        repo_stanza_name="repo_input://"+self.repo_url

        self.logger.debug("repo stanza is %s", repo_stanza_name)

        cfg = cli.getConfStanza('inputs',repo_stanza_name)
        #self.logger.debug(cfg)

        if len(cfg) > 1 :
            self.logger.debug("There was a stanza with that repo name. Branch name is %s", cfg['branch'])
            branch=cfg['branch']
            self.logger.debug("The monitored branch of this repo is %s",branch)
        else:
            self.logger.debug("There was no stanza with that repo name")

        return branch
    
    def commit_collection_update(self,field,value,repo_url,branch):

            self.logger.debug("In commit collection update")
            session_key = self._metadata.searchinfo.session_key
            splunkd_uri = self._metadata.searchinfo.splunkd_uri

            service = splunklib.client.connect(token=session_key, app="octamis_git_ucc")
            collection_name = "git_versions"
            collection = service.kvstore[collection_name]
            kv_query={"repo": repo_url ,  "branch" : branch}
            self.logger.debug(kv_query)
            git_repo=collection.data.query(query=json.dumps(kv_query))
            self.logger.debug(git_repo)

            if len(git_repo) == 0:
                self.logger.error("This is a deviating case where you're pushing to a repo you didn't input to the app!")
            else:
                self.logger.debug("Not a deviating case")
                self.logger.debug("The git repo to be updated is %s",git_repo[0])
                entry_key=git_repo[0]["_key"]
                local_commit=git_repo[0][field]
                #self.logger.debug(entry_key)

                git_version_keys = ['author', 'branch','buildhash','clienthash','commit_message','remotehash','repo']
                kv_record=[]

                for key in git_version_keys:
                    if type(git_repo[0][key]) == "bytes": 
                        kv_record.append(git_repo[0][key].decode('UTF-8')) 
                    else: 
                        kv_record.append(git_repo[0][key])
                
                it = iter(kv_record)
                kv_record = dict(zip(git_version_keys, kv_record))
                if isinstance(value, (bytes, bytearray)):
                    #self.logger.debug("value was bytes")
                    kv_record[field] = value.decode('UTF-8')
                else:
                    #self.logger.debug("value was NOT bytes")
                    kv_record[field] = value
                #self.logger.debug(kv_record)

                collection.data.update(entry_key,json.dumps(kv_record))




    def transform(self, records):

        cfg = cli.getConfStanza('octamis_git_ucc_settings',"logging")
        self.logger.info("LogLevel is set to %s",cfg['loglevel'])
        self.logger.debug('Sneakygit: %s', self)

        if self.action=="show_commits":

            if not self.branch:
                branch=self.get_branch(self.repo_url)
            else:
                branch = self.branch

            if branch == "":
                self.logger.debug("ERROR: The repo " + self.repo_url + " is not stored and therefore there was no branch to set for collection" + "outcome Failed")
                yield {'_time': time.time(),'response': "ERROR: The repo " + self.repo_url + " is not stored and therefore there was no branch to set for collection", "outcome": "Failed"}
                sys.exit()
            else:

                remote_path, git_user, auth_type, gitclient =self.identify_auth()
                self.logger.debug("In show_commits action")
                self.logger.debug("COMMITS REMOTE PATH IS %s",remote_path)

                if not git_user and self.fallback_user_default == "True":
                    self.logger.debug("No stored bitbucket user was found for " +  self._metadata.searchinfo.username + ". No default_user was found either")
                    yield {'_time': time.time(),'response': "ERROR: No stored bitbucket user was found for " +  self._metadata.searchinfo.username + ". No default_user was found either", "outcome": "Failed"}
                    sys.exit()
                elif not git_user:
                    self.logger.debug("No stored bitbucket user was found for " +  self._metadata.searchinfo.username + ". fallback_user_default=\"True\" is an option to overcome the issue")
                    yield {'_time': time.time(),'response': "ERROR: No stored bitbucket user was found for " +  self._metadata.searchinfo.username, "outcome": "Failed"}
                    sys.exit()

                commits=self.show_commits(remote_path,gitclient,branch)
                #self.logger.debug(commits)
                if not commits:
                    # We're trying to be friendly and refresh the access_token
                    self.logger.debug("no commits")     
                    self.gitfetch(remote_path,auth_type,gitclient,git_user,branch)
                    remote_path, git_user, auth_type, gitclient =self.identify_auth()
                    commits=self.show_commits(remote_path,gitclient,branch)
                if not commits:
                    yield {'_time': time.time(),'Message': "Token refresh did not provide connectivity. Either the repo doesn't exist, or doesn't have commits, or there's a network error"}     
                for c in commits:
                    yield {'_time': time.time(), 'hash': c[0].decode('UTF-8'), "commit_message": c[1].decode('UTF-8'), "author": c[2].decode('UTF-8'), "repo": self.repo_url, "committer": c[3].decode('UTF-8'), "author_time": c[4], "branch": c[5] }


        elif self.action=="token_break_test":
            self.logger.debug("IN TOKEN token_break_test")
            session_key=self._metadata.searchinfo.session_key

            c=credentials.CredentialManager(session_key,'octamis_git_ucc',realm='__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account')
            #c.set_password("test","{\"access_token\": \"" + "xxxxxxxxxx" + "\", \"client_secret\": \"" + "JMBsV4sRpjDG6ztzD63GAVyC66njDx9p" + "\", \"refresh_token\": \"" + "FstV5mms8cLZVBGqwN" + "\"}") 
            c.delete_password("test")
            #c.get_password("test")
            #yield {'_time': time.time(), 'pass': c.get_password("test") }


        elif self.action=="dump_creds":
            lookup_path=os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'octamis_git_ucc', 'lookups','creds.csv')
            self.logger.debug(lookup_path)
            #collection = self.service.kvstore["sec_dump"]
            session_key=self._metadata.searchinfo.session_key
            c=credentials.CredentialManager(session_key,'octamis_git_ucc',realm='__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account')
            creds=c._get_all_passwords()
            with open(lookup_path, 'w+', newline='') as csvfile:
                spamwriter = csv.writer(csvfile)
            for cred in creds:
                if cred["realm"] == "__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account":
                    #self.logger.debug(cred["clear_password"])
                    #collection.data.insert(json.dumps({"user": cred["username"], "creds": cred["clear_password"]}))
                    with open(lookup_path, 'a', newline='') as csvfile:
                        spamwriter = csv.writer(csvfile)
                        spamwriter.writerow([cred["username"], cred["clear_password"]])


        elif self.action=="restore_creds":
            session_key=self._metadata.searchinfo.session_key
            #collection = self.service.kvstore["sec_dump"]
            lookup_path=os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'octamis_git_ucc', 'lookups','creds.csv')
            with open(lookup_path, 'r', newline='') as csvfile:
                csv_reader = csv.reader(csvfile, delimiter=',')
                c=credentials.CredentialManager(session_key,'octamis_git_ucc',realm='__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account')
                for row in csv_reader:
                    self.logger.debug(row[0])
                    self.logger.debug(row[1])
                    temp=json.loads(row[1])
                    self.logger.debug(type(temp))
                    c.set_password(row[0],"{\"access_token\": \"" + temp.get("access_token") + "\", \"client_secret\": \"" + temp.get('client_secret') + "\", \"refresh_token\": \"" + temp.get('refresh_token') + "\"}") 
            os.remove(lookup_path)
            #self.logger.debug(collection.data.query())
            #dump=collection.data.query()
            #for d in dump:
            #    temp=d["creds"]
            #    #self.logger.debug(d["creds"])
            #    temp=json.loads(temp)
            #    #self.logger.debug(type(temp))
            #    c=credentials.CredentialManager(session_key,'octamis_git_ucc',realm='__REST_CREDENTIAL__#octamis_git_ucc#configs/conf-octamis_git_ucc_account')
            #    c.set_password(d["user"],"{\"access_token\": \"" + temp.get("access_token") + "\", \"client_secret\": \"" + "JMBsV4sRpjDG6ztzD63GAVyC66njDx9p" + "\", \"refresh_token\": \"" + "FstV5mms8cLZVBGqwN" + "\"}") 





        elif self.action=="reset_tracking":
            session_key=self._metadata.searchinfo.session_key

            HOST = "localhost"
            PORT = 8089
            service = splunklib.client.connect(token=session_key, app="octamis_git_ucc")
            jobs = service.jobs

            # Run a blocking search--search everything, return 1st 100 events
            kwargs_blockingsearch = {"exec_mode": "blocking"}
            #searchquery_blocking = "| inputlookup git_repos | rename repo as r, branch as b | map search=\"| sneakygit action=\"head\" repo_url=\"$r$\" branch=\"$b$\" | eval buildhash = \\\"$buildhash$\\\", clienthash = \\\"$buildhash$\\\"\" | fields - _time | table * | outputlookup git_versions"
            searchquery_blocking = "| rest /servicesNS/-/-/configs/conf-inputs | where 'eai:appName' = \"octamis_git_ucc\" AND title LIKE \"repo_input://%\" | rex field=title \"repo_input://(?<repo>.*)\" | rename hash as buildhash | table repo, branch, buildhash | outputlookup git_repos | rename repo as r, branch as b | map search=\"| sneakygit action=\"head\" repo_url=\"$r$\" branch=\"$b$\" | eval buildhash = \\\"$buildhash$\\\", clienthash = \\\"$buildhash$\\\"\" | fields - _time | table * | outputlookup git_versions"
            job = jobs.create(searchquery_blocking, **kwargs_blockingsearch)



        elif self.action=="initialize_git_repos":
            session_key=self._metadata.searchinfo.session_key
            HOST = "localhost"
            PORT = 8089
            service = splunklib.client.connect(token=session_key, app="octamis_git_ucc")
            jobs = service.jobs

            # Run a blocking search--search everything, return 1st 100 events
            kwargs_blockingsearch = {"exec_mode": "blocking"}
            #searchquery_blocking = "| inputlookup git_repos | rename repo as r, branch as b | map search=\"| sneakygit action=\"head\" repo_url=\"$r$\" branch=\"$b$\" | eval buildhash = \\\"$buildhash$\\\", clienthash = \\\"$buildhash$\\\"\" | fields - _time | table * | outputlookup git_versions"
            searchquery_blocking = "| rest /servicesNS/-/-/configs/conf-inputs | where 'eai:appName' = \"octamis_git_ucc\" AND title LIKE \"repo_input://%\" | rex field=title \"repo_input://(?<repo>.*)\" | rename hash as buildhash | table repo, branch, buildhash | outputlookup git_repos"
            job = jobs.create(searchquery_blocking, **kwargs_blockingsearch)

        elif self.action=="init_repo_from_file":
            session_key=self._metadata.searchinfo.session_key
            HOST = "localhost"
            PORT = 8089
            service = splunklib.client.connect(token=session_key, app="octamis_git_ucc")
            jobs = service.jobs

            # Run a blocking search--search everything, return 1st 100 events
            kwargs_blockingsearch = {"exec_mode": "blocking"}
            #searchquery_blocking = "| inputlookup git_repos | rename repo as r, branch as b | map search=\"| sneakygit action=\"head\" repo_url=\"$r$\" branch=\"$b$\" | eval buildhash = \\\"$buildhash$\\\", clienthash = \\\"$buildhash$\\\"\" | fields - _time | table * | outputlookup git_versions"
            searchquery_blocking = "| inputlookup newrepos.csv "
            job = jobs.create(searchquery_blocking, **kwargs_blockingsearch)
            rr = results.ResultsReader(job.results())
            conf=service.confs['inputs']
            self.logger.debug("CREATING STANZA")
            for result in rr:
                flag = 0
                self.logger.debug(result['repo'])
                repo_stanza_name = "repo_input://"+result['repo']
                try:
                    cfg = cli.getConfStanza('inputs',repo_stanza_name)
                    #self.logger.debug(cfg)
                    self.logger.debug("GOT THE STANZA")
                except Exception as e:
                    self.logger.debug("This repo was not yet stored")
                    flag = 1
                    pass
                if flag == 1:
                    try:
                        stanza = conf.create(repo_stanza_name)
                        stanza.submit({"branch": result['branch'], "hash" : result['buildhash']})
                    except Exception as e:
                        pass

            #searchquery_blocking = "| inputlookup newrepos.csv | outputlookup git_repos "
            searchquery_blocking = "| inputlookup newrepos.csv | outputlookup git_repos | rename repo as r, branch as b | map search=\"| sneakygit action=\\\"head\\\" repo_url=\\\"$r$\\\" branch=\\\"$b$\\\" | eval buildhash = \\\"$buildhash$\\\", clienthash = \\\"$buildhash$\\\"\" | fields - _time | table * | join type=left repo     [| inputlookup git_versions     | eval origin_hash=clienthash ] | eval clienthash=coalesce(origin_hash,clienthash) | fields - origin_hash | table * | outputlookup git_versions"
            job = jobs.create(searchquery_blocking, **kwargs_blockingsearch)


        elif self.action == "head":
            if not self.branch:
                branch=self.get_branch(self.repo_url)
            else:
                branch = self.branch
            if branch == "":
                yield {'_time': time.time(),'response': "ERROR: The repo " + self.repo_url + " is not stored and therefore there was no branch to set for collection", "outcome": "Failed"}
                sys.exit()
            else:    
                self.logger.debug(branch)
                remote_path, git_user, auth_type, gitclient = self.identify_auth()
                self.logger.debug("after identifyauth")
                self.logger.debug(remote_path + " " + git_user + " " + auth_type + " " + gitclient)
                if len(remote_path) == 0:
                    self.logger.debug("There were no credentials to get head of the repo " + self.repo_url)
                    yield {'_time': time.time(), 'Response': "There were no credentials to get head of the repo"}
                    sys.exit()
                repo, exception, remediated, remote_path = self.gitfetch(remote_path, auth_type, gitclient, git_user, branch)
                self.logger.debug(f"finished fetch remote_path={remote_path}, auth_type={auth_type}, gitclient={gitclient}, git_user={git_user}, branch={branch}")
                head=self.show_head(repo, branch)
                self.logger.debug(f"head is {head} for {remote_path}")
                #MANAGE OAUTH CONSUMERS https://bitbucket.org/TiagoMatos1/workspace/settings/api
                # Callback URL http://tiago-xps-15-9500:8000/en-US/app/octamis_git_ucc/octamis_git_ucc_redirect
                self.logger.debug(exception)
                if len(head) > 0:
                    self.logger.debug("Head of repo " + self.repo_url + "was retrieved")
                    yield {'_time': time.time(), 'remotehash': head[0].decode('UTF-8'), "commit_message": head[1].decode('UTF-8'), "author": head[2].decode('UTF-8'), "repo": self.repo_url, "committer": head[3].decode('UTF-8'), "author_time": head[4], "branch": branch }
                elif str(exception).find("401") != -1:
                    self.logger.debug("Exception %s", exception)
                    yield {'_time': time.time(), "Response": "ERROR: Exception 401 occured. Please ensure you have credentials to access the repository and it is correct." }
                else:
                    self.logger.debug("There was no previous tree, this is an initial commit into repo ")
                    yield {'_time': time.time(), "Response": "There was no previous tree, this is an initial commit into repo " + self.repo_url + " on branch " + branch }



        elif self.action == "check_auth":
            remote_path, git_user, auth_type, gitclient =self.identify_auth()
            #self.logger.debug("GIT USER: %s", git_user)
            self.logger.debug("After identifyauth")
            #self.logger.debug(len(git_user))

            if len(git_user) > 0:
                yield {'_time': time.time(), 'Status': "The user " + git_user + " is currently on " + auth_type + " authentication"}
            else:
                yield {'_time': time.time(), 'response': "No bitbucket credentials were found for the user " + self._metadata.searchinfo.username + " in passwords storage"}
                sys.exit()

        elif self.action=="push_remote":
            incoming_data=[record for record in records]

            branch = self.branch or self.get_branch(self.repo_url)

            if branch == "":
                self.postmessage(self.repo_url,branch,"Credentials Fallback","The repo " + self.repo_url + " is not stored and therefore there was no branch to set for collection","critical")
                yield {'_time': time.time(),'response': "ERROR: The repo " + self.repo_url + " is not stored and therefore there was no branch to set for collection", "outcome": "Failed"}
                sys.exit()
      
            response_text = ""
            remote_path, git_user, auth_type, gitclient=self.identify_auth()

            self.logger.debug("GIT USER IS %s",git_user)
            self.logger.debug("REMOTE PATH IS %s",remote_path)
            self.logger.debug("gitclient IS %s",gitclient)
            self.logger.debug("branch is %s",branch)
            self.logger.debug("AUTH is %s",auth_type)
          


            if len(git_user) != 0:
                self.logger.debug("User found or fallback to default user was successful")
            elif self.fallback_user_default == "True":
                self.logger.debug("FALLING BACK TO DEFAULT USER")
                response_text = "identify_auth=\"No credentials were found for user " + self._metadata.searchinfo.username + ". Fallback to default_user was enabled and used"
                self.postmessage(self.repo_url,branch,"Credentials Fallback","No credentials were found for user " + self._metadata.searchinfo.username + ". Fallback to default_user was enabled and used","warn")
            else:
                self.postmessage(self.repo_url,branch,"Credentials Missing","ERROR: No bitbucket credentials were found for the user " + self._metadata.searchinfo.username + " in passwords storage","critical")
                yield {'_time': time.time(),'response': "ERROR: No bitbucket credentials were found for the user " + self._metadata.searchinfo.username + " in passwords storage. use fallback_user_default=\"True\" to fallback to default user", "outcome": "Failed"}               
                sys.exit()
                

            # fetch into our local in memory repo from bitbucket
            errIO = io.BytesIO()
            outIO = io.StringIO()

            local_repo, fetch_except, remediated, remote_path = self.gitfetch(remote_path,auth_type,gitclient,git_user,branch)

            self.logger.debug("AFTER GITFETCH")
            self.logger.debug(fetch_except)
            self.logger.debug(local_repo)

            if len(str(fetch_except)) > 0 and not remediated:
                yield {'_time': time.time(), 'response': str(fetch_except)}
                sys.exit()  



            #repo[b'refs/heads/'+branch.encode('utf-8')] = repo[b'refs/remotes/origin/'+ branch.encode('utf-8')]
            #iterator=repo.head()    
            #head=[iterator,repo.get_object(iterator).message,repo.get_object(iterator).author,repo.get_object(iterator).committer,time.strftime("%d/%m/%Y %H:%M:%S %Z",time.localtime(repo.get_object(iterator).author_time))]




            try:
                local_repo[b'refs/heads/'+branch.encode('utf-8')] = local_repo[b'refs/remotes/origin/'+branch.encode('utf-8')]
                last_tree = local_repo[local_repo[b'HEAD'].tree]
                self.logger.debug(f"last_tree is {last_tree}")
                
                # Need the head of the fecthed repo to compare with the last saved repo version
                fetched_head=local_repo.head()
                self.logger.debug("After local_repo_head")
                #self.logger.debug("This is my Remote HEAD %s", fetched_head)
            #except Exception, e:
            except Exception as e:
                self.logger.debug("exception: err output is %s", str(e))
                self.logger.debug("There was no previous tree, this is an initial commit into repo %s", self.repo_url)
                self.postmessage(self.repo_url,branch,"No Tree","There was no previous tree on repo " + self.repo_url,"warn")
                last_tree = Tree()
                yield {'_time': time.time(),'response': "There was no previous tree, this is an initial commit into repo " + self.repo_url}
                pass


            try:
                fetched_head is not None
                check_commit=self.commit_crosshcheck(fetched_head,self.repo_url,branch)
            except Exception as e:
                #check_commit=self.commit_crosshcheck(fetched_head,self.repo_url,branch)
                self.logger.error(e)                        
                #logger.debug(check_commit == None)
                check_commit = None
                fetched_head = None
                tree_line=None

            self.logger.debug("check_commit is %s, force is %s", check_commit, self.force)

            if check_commit == "desynchronized" and self.force == "False":
                new_fetched_repo, f_exception, remediated, remote_path = self.gitfetch(remote_path, auth_type, gitclient, git_user, branch)
                self.logger.debug(f_exception)
                new_fetched_repo[b'refs/heads/'+branch.encode('utf-8')] = new_fetched_repo[b'refs/remotes/origin/'+branch.encode('utf-8')]
                new_fetched_head=new_fetched_repo.head()
                self.logger.debug("This is my NEW HEAD %s", new_fetched_head)
                self.commit_collection_update("remotehash",new_fetched_head,self.repo_url,branch)
                self.postmessage(self.repo_url,branch,"Repo Desync","Remote repo version is different from the client version. Use force=\"True\" at your own risk","warn")
                #yield {'_time': time.time(),'response': "Remote repo version is different from the one used to build the Splunk instance. If you want to ignore this, run the command with and extra argument force=True. Exiting"}
                response_text = response_text + " check_commit=Remote repo version is different from the one used to build the Splunk instance "
                yield {'_time': time.time(),'response': response_text, "outcome": "Failed"}
                sys.exit()
            elif (check_commit == "desynchronized" and self.force == "True"):
                self.postmessage(self.repo_url,branch,"Repo Desync Forced","Forced push to remote", "critical")
                response_text = response_text + "check_commit=\"Be aware you forced push to remote when version is different from the client version\", "
                #yield {'_time': time.time(),'response': "Be aware you forced push to remote when version is different from the one used to build the Splunk instance."}
            elif check_commit is None and self.force == "True":
                #yield {'_time': time.time(),'response': "You're trying to push to a repo that isn't monitored by Octamis Git Tools, and you just forced it. We can't guarantee data consistency from this moment on."}
                response_text = response_text + " check_commit=You're trying to push to a repo that isn't monitored by Octamis Git Tools, and you just forced it. We can't guarantee data consistency from this moment on, "
                #sys.exit()
            elif check_commit is None :
                yield {'_time': time.time(),'response': "You're trying to push to a repo that isn't monitored by Octamis Git Tools. Please add it and try again","outcome": "Failed"}
                sys.exit()
                



            new_full_data = []
            csv_columns = set()
            existing_fields = []
            leaf_tree=None
            file_name=None
            tree_line=None
            if gitclient == "codecommit":
                # TODO Implement existing file read in codecommit
                pass
            else:
                def to_tree_line(path, root_tree, repo):
                    self.logger.debug("to_tree_line(%s, root_tree, repo) called" % (path))
                    self.logger.debug(f"root_tree is {root_tree}")
                    self.logger.debug(f"root_tree contents are {root_tree.items()}")

                    folderLine = path.split("/")[0:-1:1]
                    theFile = path.split("/")[-1]
                    treeLine = [{"name": "", "tree": root_tree}]
                    parentContext = root_tree

                    for folder in folderLine:
                        self.logger.debug(f"About to get {parentContext[folder.encode()][1]} from {repo}")
                        tree = repo.get_object(parentContext[folder.encode()][1])
                        treeLine += [{"name": folder, "tree": tree}]
                        parentContext = tree
                    self.logger.debug("toTreeLine to return (%s, %s)" % (treeLine, theFile))
                    return treeLine, theFile

                def read_csv(file_name, parent_tree, repo):
                    existing_blob_ref = None
                    contents = []
                    fieldNames = []
                    try:
                        existing_blob_ref = parent_tree[file_name.encode()]
                    except KeyError as ke:
                        self.logger.debug("Could not find file called %s" % file_name)
                    if existing_blob_ref is not None:
                        existing_blob = repo.get_object(existing_blob_ref[1])
                        blob_read = io.StringIO(existing_blob.as_raw_string().decode("utf-8"))
                        reader = csv.DictReader(blob_read)
                        fieldNames=reader.fieldnames
                        for row in reader:
                            contents.append(row)
                    return contents, fieldNames

                def remove_sperceded_data(original_data, new_data, pk_field):
                    indexed = {rec[pk_field]: rec for rec in original_data}
                    surviving_data = original_data[:]

                    for record in new_data:
                        pk = record[pk_field]
                        if pk in indexed:
                            surviving_data.remove(indexed.get(pk))

                    return surviving_data


                tree_line, file_name = to_tree_line(self.file, last_tree, local_repo)
                leaf_tree = tree_line[-1]["tree"]
                existing_data = []
                if self.write_mode != "replace":
                    (existing_data, existing_fields)=read_csv(file_name, leaf_tree, local_repo)
                    self.logger.debug("existing_data read")
                new_full_data += existing_data if self.write_mode != "upsert" else remove_sperceded_data(existing_data, incoming_data, self.upsert_pk)

            for record in incoming_data:
                new_full_data.append(record)

            for record in new_full_data:
                csv_columns |= set(record.keys())
                
            response_text = response_text + " glue_operation=\"Successfully added records from above search to a variable\", "
            #yield {'_time': time.time(),'response': "glue_operation: Successfully added records from above search to a variable"}

            self.logger.debug("left records glue")
            self.logger.debug("after csv_columns")
            mem_file = io.StringIO()
            try:
                prior_cols=existing_fields if existing_fields else []
                writer = csv.DictWriter(mem_file, fieldnames=list(prior_cols)+list(sorted(csv_columns-set(prior_cols))))
                writer.writeheader()
                for data in new_full_data:
                    #self.logger.debug(data)    
                    writer.writerow(data)
            except IOError:
                self.logger.debug("IOError")
                response_text = response_text + " dict_to_csv=\"Success\", " 
            #yield {'_time': time.time(),'response': "Dict to CSV: Success"}


            self.logger.debug("response': \"Dict to CSV: Success\"")

            
            byte_str=mem_file.getvalue()
            byte_str=byte_str.encode('UTF-8')
            text_obj = byte_str.decode('UTF-8')

            self.logger.debug(text_obj)

            head=self.show_head(local_repo, branch)
            self.logger.debug(head[0])

            if gitclient == "codecommit":
                pattern = re.compile('url:git-codecommit.eu-west-2.amazonaws.com/v1/repos/(.*)\|\|password:(.*)\|\|clientid:(.*)\|\|secret:(.*)')
                match = pattern.match(remote_path)
                aws_repo = match.group(1)
                password = match.group(2)
                clientid = match.group(3)
                secret = match.group(4)

                client = boto3.client(
                    'codecommit',
                    aws_access_key_id=clientid,
                    aws_secret_access_key=secret
                    )

                response = client.put_file(
                    repositoryName=aws_repo,
                    branchName=branch,
                    fileContent=bytes(text_obj,'utf-8'),
                    filePath=self.file,
                    fileMode='NORMAL',
                    parentCommitId=head[0],
                    commitMessage=self.message,
                    name=self._metadata.searchinfo.username,
                    email=self.email)



                response_text = response_text + " commit_operation=\"Success\", "
                response_text = response_text + " push_operation=\"Success\", "

            else:
                def createFile(contents, repo):
                    self.logger.debug("createFile(%s, %s) called" % (contents, repo))
                    blob = Blob.from_string(bytes(contents, 'utf-8'))
                    repo.object_store.add_object(blob)
                    self.logger.debug("createFile to return %s" % (blob))
                    return blob

                def addBlobToTree(blob, fileName, tree, repo):
                    self.logger.debug("addBlobToTree called")
                    tree.add(bytes(fileName, 'utf-8'), 0o100644, blob.id)
                    repo.object_store.add_object(tree)
                    self.logger.debug("addBlobToTree done")

                def rippleTreeLeafUpdate(treeLine, leaf, repo):
                    self.logger.debug("rippleTreeLeafUpdate(%s, %s, %s) called" % (treeLine, leaf, repo))
                    leafName = treeLine[-1]["name"]
                    for aboutTree in treeLine[-2::-1]:
                        tree, treeName = aboutTree["tree"], aboutTree["name"]
                        tree.add(leafName.encode(), stat.S_IFDIR, leaf.id)
                        repo.object_store.add_object(tree)
                        leafName, leaf = treeName, tree
                    self.logger.debug("rippleTreeLeafUpdate done")


                new_blob = createFile(text_obj, local_repo)
                self.logger.debug("leaf_tree is %s" % leaf_tree)
                addBlobToTree(new_blob, file_name, leaf_tree, local_repo)
                rippleTreeLeafUpdate(tree_line, leaf_tree, local_repo)

                self.logger.debug("response': add object finished")

                self.logger.debug(remote_path)
                self.logger.debug("push_remote: about to do_commit")
                try:            
                    local_repo.do_commit(
                                message=bytes(self.message, 'ascii'),
                                ref=b'refs/heads/'+branch.encode('ascii'),
                                tree=last_tree.id)
                except Exception as ex:
                    self.logger.debug("push_remote: local commit failed")
                    self.logger.debug(ex)
                    raise ex
                else:
                    self.logger.debug("push_remote: local commit successful")
                finally:
                    self.logger.debug("push_remote: local commit done")
                self.logger.debug("After do_commit")
                self.logger.debug(local_repo)
                self.logger.debug(remote_path)
                self.logger.debug(type(remote_path))
                response_text = response_text + " commit_operation=\"Success\", "
                #yield {'_time': time.time(),'response': "Commit Operation: Success"}
                response_text = response_text + " push_operation=\"Success\", "
                self.logger.debug("push_remote: about to porcelain.push")
                try:
                    porcelain.push(local_repo, str(remote_path), branch,outstream=outIO, errstream=errIO)
                except Exception as ex:
                    self.logger.debug("push_remote: porcelain.push failed")
                    self.logger.debug(ex)
                    raise ex
                else:
                    self.logger.debug("push_remote: porcelain.push successful")
                finally:
                    self.logger.debug("push_remote: porcelain.push done")
                #yield {'_time': time.time(),'response': "Push Remote Operation: Success"}
                self.logger.debug("After porcelain_push")

            


            new_fetched_repo, f_exception, remediated, remote_path = self.gitfetch(remote_path, auth_type, gitclient, git_user, branch)

            self.logger.debug(f_exception)
            new_fetched_repo[b'refs/heads/'+branch.encode('utf-8')] = new_fetched_repo[b'refs/remotes/origin/'+branch.encode('utf-8')]
            new_fetched_head=new_fetched_repo.head()
            self.logger.debug("This is my NEW HEAD %s", new_fetched_head)


            # Updating the git versions kvstore with the new hash
            self.postmessage(self.repo_url,branch,"Version Pushed"," Client Version pushed to remote", "warn")

            if check_commit == "synchronized" or self.force == "True":
                self.commit_collection_update("clienthash",new_fetched_head,self.repo_url,branch)
                self.commit_collection_update("remotehash",new_fetched_head,self.repo_url,branch)
            else:
                self.commit_collection_update("remotehash",new_fetched_head,self.repo_url,branch)
            response_text = response_text + " refresh_hash=\"Success\" "
            yield {'_time': time.time(),'response': response_text, "outcome": "Success"}
        else: 
            yield {'_time': time.time(),'response': "This is not a recognized action. Please ensure you are providing push_remote as action argument.", "outcome": "Failed"}

    _globals = {'__builtins__': __builtins__}

dispatch(SneakyGit, sys.argv, sys.stdin, sys.stdout, __name__)