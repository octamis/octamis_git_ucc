import import_declare_test
import sys
import json

from splunklib import modularinput as smi

class REPO_INPUT(smi.Script):

    def __init__(self):
        super(REPO_INPUT, self).__init__()

    def get_scheme(self):
        scheme = smi.Scheme('repo_input')
        scheme.description = 'Repository'
        scheme.use_external_validation = True
        scheme.streaming_mode_xml = True
        scheme.use_single_instance = True

        scheme.add_argument(
            smi.Argument(
                'name',
                title='Name',
                description='Name',
                required_on_create=True
            )
        )
        
        scheme.add_argument(
            smi.Argument(
                'hash',
                required_on_create=True,
            )
        )
        
        scheme.add_argument(
            smi.Argument(
                'branch',
                required_on_create=True,
            )
        )
        
        scheme.add_argument(
            smi.Argument(
                'example_help_link',
                required_on_create=False,
            )
        )
        
        return scheme

    def validate_input(self, definition):
        return

    def stream_events(self, inputs, ew):
        input_items = [{'count': len(inputs.inputs)}]
        for input_name, input_item in inputs.inputs.items():
            input_item['name'] = input_name
            input_items.append(input_item)
        event = smi.Event(
            data=json.dumps(input_items),
            sourcetype='repo_input',
        )
        ew.write_event(event)


if __name__ == '__main__':
    exit_code = REPO_INPUT().run(sys.argv)
    sys.exit(exit_code)