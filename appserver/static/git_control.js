
require.config({
  paths: {
    theme_utils: '../app/octamis_git_ucc/theme_utils'
  }
});
require([
    "splunkjs/mvc",
    "splunkjs/mvc/searchmanager",
    "splunkjs/mvc/postprocessmanager",
    "splunkjs/mvc/eventsviewerview",
    "splunkjs/mvc/chartview",
    "splunkjs/mvc/tableview",
    "splunkjs/mvc/splunkmapview",
	"splunkjs/mvc/tokenutils",
    "splunkjs/mvc/radiogroupview",
	"splunkjs/mvc/multidropdownview",
	"splunkjs/mvc/simpleform/input/dropdown",
	"splunkjs/mvc/simpleform/input/text",
	"splunk.util",
	"underscore",
	"jquery",
	"theme_utils",
	"splunkjs/mvc/simplexml/ready!",
], function(
	mvc,
    SearchManager,
    PostProcessManager,
    EventsViewer,
    ChartView,
    TableView,
    SplunkMapView,
	TokenUtils,
	RadioGroupView,
	MultiDropdownView,
	DropdownInput,
	TextInput,
	splunkUtil,
	_,
	$,
	themeUtils,
) {	
   
		//console.log("About to start setup");

		var defaultTokenModel = mvc.Components.get("default");
	    var submittedTokenModel = mvc.Components.get("submitted");
	    var envTokenModel = mvc.Components.get('env');
	    var instance = mvc.Components.getInstance("default")


        defaultTokenModel.set("repo","");
        submittedTokenModel.set("repo","");

        defaultTokenModel.set("auth_token","basic");
        submittedTokenModel.set("auth_token","basic");


	    var _ = require('underscore');
	    var $ = require('jquery');
	    var mvc = require('splunkjs/mvc');
	    var SimpleSplunkView = require('splunkjs/mvc/simplesplunkview');
	    var splunkUtil = require('splunk.util');

		var auth_search_table_only = new SearchManager({
		    id: "auth_search_table_only",
		    preview: true,
		    cache: true,
		    status_buckets: 300,
		    search: "| sneakygit action=\"check_auth\" repo_url=\"$repo$\"",
			app: "octamis_git_ucc",
			autostart: true,
			}, {tokens: true});
         
		var auth_search = new SearchManager({
		    id: "auth_search",
		    preview: true,
		    cache: true,
		    status_buckets: 300,
		    search: "| sneakygit action=\"check_auth\" repo_url=\"$repo$\"",
			app: "octamis_git_ucc",
			autostart: false,
			}, {tokens: true});

		var auth_table = new TableView ({
			id: "auth_type_table",
			managerid: "auth_search_table_only",
			el: $("#auth_type_table"),
			drilldown: "none"
			});

		var repos_dropdown_search = new SearchManager({
		    id: "repos_dropdown_search",
		    preview: true,
		    cache: true,
		    status_buckets: 300,
		    search: "| inputlookup git_repos | dedup repo | table repo",
			app: "octamis_git_ucc",
			autostart: true,
			}, {tokens: true});

		var repos_table = new SearchManager({
		    id: "repos_table",
		    preview: true,
		    cache: true,
		    status_buckets: 300,
		    search: "| inputlookup git_repos",
			app: "octamis_git_ucc",
			autostart: true,
			}, {tokens: true});

        var repos_dropdown_viz = new DropdownInput({
            id: "repos_dropdown_viz",
            managerid: "repos_dropdown_search",
            labelField: "repo",
            valueField: "repo",
            el: $("#repos_dropdown_viz"),
        	"value": "$form.repos_dropdown_viz$"},
        	{tokens: true}).render();

        var repos_table_viz = new TableView({
            id: "repos_table_viz",
            managerid: "repos_table",
            labelField: "repo",
            valueField: "repo",
            el: $("#repos_table_viz"),
        	"value": "$form.repos_table_viz$",
            "drilldown": "none",
            "wrap": "true"},
        	{tokens: true}).render();


		var rebuild_versions = new SearchManager({
		    id: "rebuild_versions",
		    preview: true,
		    cache: true,
		    status_buckets: 300,
		    //search: "| inputlookup git_versions | eval localHash=hash | rename repo as r, branch as b",
		    search: "| sneakygit action=\"head\" repo_url=\"$new_repo$\" branch=\"$repo_branch$\" | eval clienthash = remotehash, buildhash = remotehash | fields - _time | table * | outputlookup append=true git_versions",
			app: "octamis_git_ucc",
			autostart: false,
			}, {tokens: true});

		var build_versions_init = new SearchManager({
		    id: "build_versions_init",
		    preview: true,
		    cache: true,
		    status_buckets: 300,
		    search: "| inputlookup git_repos | rename repo as r, branch as b | map search=\"| sneakygit action=\"head\" repo_url=\"$$r$$\" branch=\"$$b$$\" | eval buildhash = \\\"$$buildhash$$\\\", clienthash = \\\"$$buildhash$$\\\"\" | fields - _time | table * | join type=left repo [ | inputlookup git_versions | eval origin_hash=clienthash ] | eval clienthash=coalesce(origin_hash,clienthash) | fields - origin_hash | table * | outputlookup git_versions",
			app: "octamis_git_ucc",
			autostart: false,
			}, {tokens: true});


		var latest_versions_search = new SearchManager({
			id: "latest_versions_search",
		    preview: true,
		    cache: true,
		    status_buckets: 300,
		    search: "| inputlookup git_versions | eval Status=case(clienthash!=remotehash,\"Desync\",buildhash!=clienthash,\"Changed\",buildhash==clienthash,\"Current\") | table Status, * | where isnotnull(repo)",
			app: "octamis_git_ucc",
			autostart: true,
			}, {tokens: true});

        
        var latest_versions_table = new TableView({
            id: "latest_versions_table",
            managerid: "latest_versions_search",
            labelField: "repo",
            valueField: "repo",
            el: $("#latest_versions_table"),
        	"value": "$form.latest_versions_table$",
        	"drilldown": "none",
            "wrap": "true"},
        	{tokens: true}).render();


		var show_commits_search = new SearchManager({
		    id: "show_commits_search",
		    preview: true,
		    cache: true,
		    status_buckets: 300,
		    //search: "| inputlookup git_versions | eval localHash=hash | rename repo as r, branch as b",
		    search: "| sneakygit action=\"show_commits\" repo_url=\"$repo$\" branch=\"$branch$\" | fields - _time | table *",
			app: "octamis_git_ucc",
			autostart: true,
			}, {tokens: true});


        var show_commits_table = new TableView({
            id: "show_commits_table",
            managerid: "show_commits_search",
            el: $("#show_commits_table"),
        	"value": "$form.show_commits_table$",
        	"drilldown": "none",
            "wrap": "true"},
        	{tokens: true}).render();




    var ICONS = {
        Changed: 'alert-circle',
        Current: 'check-circle',
        Desync: 'alert'
    };

    var RangeMapIconRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            // Only use the cell renderer for the range field
            return cell.field === 'Status';
        },
        render: function($td, cell) {
            var icon = 'question';auth_type_table
            var isDarkTheme = themeUtils.getCurrentTheme && themeUtils.getCurrentTheme() === 'dark';
            // Fetch the icon for the value
            if (ICONS.hasOwnProperty(cell.value)) {
                icon = ICONS[cell.value];
            }
            // Create the icon element and add it to the table cell
            $td.addClass('icon').html(_.template('<i class="icon-<%-icon%> <%- range %> <%- isDarkTheme %>" title="<%- range %>"></i>', {
                icon: icon,
                range: cell.value,
                isDarkTheme: isDarkTheme ? 'dark' : ''
            }));
        }
    });

    	latest_versions_table.addCellRenderer(new RangeMapIconRenderer())


        repos_dropdown_viz.on("change", function(newValue) {
            defaultTokenModel.set("repo",repos_dropdown_viz.settings.attributes.value);
            submittedTokenModel.set("repo",repos_dropdown_viz.settings.attributes.value);
        });


        var branch_input = new TextInput({
            "id": "branch_input",
            "searchWhenChanged": true,
            "value": "$form.branch_input$",
            "el": $('#branch_input'),
            "default": 'master'
        }, {
            tokens: true
        }).render();


        branch_input.on("change", function(newValue) {
            defaultTokenModel.set("branch",branch_input.settings.attributes.value);
            submittedTokenModel.set("branch",branch_input.settings.attributes.value);
        });


    service = mvc.createService({
        owner: "nobody"
    });

 document.getElementById("reset_tracking").innerHTML =  
        `<button type="submit" class="btn btn-primary" id="reset_tracking" data-toggle="tooltip" data-placement="top" title="Fetch head of each repo/branch and set them as baseline">Reset Tracking</button>`

 
 document.getElementById("global_refresh").innerHTML =  
        `<button type="submit" class="btn btn-primary" id="g_refresh" data-toggle="tooltip" data-placement="top" title="Refresh all panels in the dashboard">Refresh Panels</button>`

 document.getElementById("buildhash_recompute").innerHTML =  
        `<button type="submit" class="btn btn-primary" id="buildhash_recompute" data-toggle="tooltip" data-placement="top" title="Refresh all panels in the dashboard">Refresh Repos List</button>`



$('#buildhash_recompute').on("click",function(event){



	console.log("Clicked to Include all Repos");


 recompute_repos_in_progress = 
        `<div class="modal fade" id="refresh_versions_in_progress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Repos are being updated from the inputs</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>Please wait until this windows closes automatically</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-lg disabled" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>`

 recompute_repos_in_progress_success = 
        `<div class="modal fade" id="recompute_repos_in_progress_success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Success!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>Remote repos list has been refreshed!</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button> 
      </div>
    </div>
  </div>
</div>`


	$('body').prepend(recompute_repos_in_progress);
	$('#recompute_repos_in_progress').modal('show');


	var refresh_repos = '| rest /servicesNS/-/-/configs/conf-inputs | where \'eai:appName\' = \"octamis_git_ucc\" AND title LIKE \"repo_input://%\" | rex field=title \"repo_input://(?<repo>.*)\" | rename hash as buildhash | table repo, branch, buildhash | outputlookup git_repos'


	    var searchParams = {
	    exec_mode: "blocking",
	    earliest_time: "-60m",
	    latest_time: "now",
	};




    var service = mvc.createService({
        owner: "nobody",
        app: "octamis_git_ucc"
    });
// Create the search, wait for it to finish, and get back a job
			service.search(
	        refresh_repos,
	        searchParams,
	        function(err, job) {

	                job.track({
	                    period: 200
	                }, {
	                    done: function(job) {
	                    	console.log("The search fresh_start has finished")

	                    	build_versions_init.startSearch();

                            $('#recompute_repos_in_progress').modal('hide');

	                    	$('body').prepend(recompute_repos_in_progress_success);
                            $('#recompute_repos_in_progress_success').modal('show');

                            setTimeout(
			                function() 
			                {$('#recompute_repos_in_progress_success').modal('hide');},
			                 3000);



			           
	                    },
	                    failed: function(properties) {
							console.log("The search has failed")
						},
	                    error: function(err) {
							console.log("The error is")
	                    }
				                });	            
	       });	



});








$('#versions_refresh').on("click",function(event){



	console.log("Clicked to Refresh Versions");


 refresh_versions_in_progress = 
        `<div class="modal fade" id="refresh_versions_in_progress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Remote versions are being updated from cloud</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>Please wait until this windows closes automatically</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-lg disabled" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>`

 refresh_versions_success = 
        `<div class="modal fade" id="refresh_versions_success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Success!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>Remote versions have been refreshed!</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button> 
      </div>
    </div>
  </div>
</div>`


	$('body').prepend(refresh_versions_in_progress);
	$('#refresh_versions_in_progress').modal('show');


	var refresh_versions = '| inputlookup git_versions | rename repo as r, branch as b | map search=\"| sneakygit action=\"head\" repo_url=\\\"$r$\\\" branch=\\\"$b$\\\" | table repo,branch, remotehash\" | join type=inner repo,branch [ | inputlookup git_versions | fields - remotehash | table * ] | fields - _time | table * | outputlookup git_versions'


	    var searchParams = {
	    exec_mode: "blocking",
	    earliest_time: "-60m",
	    latest_time: "now",
	};




    var service = mvc.createService({
        owner: "nobody",
        app: "octamis_git_ucc"
    });
// Create the search, wait for it to finish, and get back a job
			service.search(
	        refresh_versions,
	        searchParams,
	        function(err, job) {

	                job.track({
	                    period: 200
	                }, {
	                    done: function(job) {
	                    	console.log("The search fresh_start has finished")

	                    	latest_versions_search.startSearch();

                            $('#refresh_versions_in_progress').modal('hide');

	                    	$('body').prepend(refresh_versions_success);
                            $('#refresh_versions_success').modal('show');

                            setTimeout(
			                function() 
			                {$('#refresh_versions_success').modal('hide');},
			                 3000);



			           
	                    },
	                    failed: function(properties) {
							console.log("The search has failed")
						},
	                    error: function(err) {
							console.log("The error is")
	                    }
				                });	            
	       });	



});



$('#reset_tracking').on("click",function(event){




 var reset_tracking_in_progress = `<div class="modal fade" id="reset_tracking_in_progress"> 
  <div class="modal-dialog model-sm"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> 
        <h4 class="modal-title">Resetting all repos baselines!</h4> 
      </div> 
      <div class="modal-body"> 
        <p>Please wait until your repos and versions are being reset to the latest remote versions</p> 
      </div> 
      <div class="modal-footer"> 
        <button type="button" class="btn btn-secondary btn-lg disabled">OK</button> 
      </div> 
    </div> 
  </div> 
</div>`


 var reset_tracking_success = `<div class="modal fade" id="reset_tracking_success"> 
  <div class="modal-dialog model-sm"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> 
        <h4 class="modal-title">Reset Done!</h4> 
      </div> 
      <div class="modal-body"> 
        <p>All repos baselines reset to the latest remote versions!</p> 
      </div> 
      <div class="modal-footer"> 
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button> 
      </div> 
    </div> 
  </div> 
</div>`

	$('body').prepend(reset_tracking_in_progress);
    $('#reset_tracking_in_progress').modal('show');


	console.log("Clicked to reset tracking");

	var hard_reset_tracking = '| inputlookup git_versions | rename repo as r, branch as b | map search=\"| sneakygit action=\"head\" repo_url=\\\"$r$\\\" branch=\\\"$b$\\\" | eval clienthash = remotehash\" | join type=inner repo,branch [ | inputlookup git_repos | table repo, branch, buildhash ] | fields - _time | table * | outputlookup git_versions'
    
    var searchParams = {
	    exec_mode: "blocking",
	    earliest_time: "-60m",
	    latest_time: "now"
	};




    var service = mvc.createService({
        owner: "nobody"
    });
// Create the search, wait for it to finish, and get back a job
			service.search(
	        hard_reset_tracking,
	        searchParams,
	        function(err, job) {

	                job.track({
	                    period: 200
	                }, {
	                    done: function(job) {
	                    	console.log("The search hard_reset_tracking has finished")

    						$('#reset_tracking_in_progress').modal('hide');

    						$('body').prepend(reset_tracking_success);
                            $('#reset_tracking_success').modal('show');
                            setTimeout(
			                function() 
			                {$('#reset_tracking_success').modal('hide');},
			                 3000);

	                    	latest_versions_search.startSearch();

			           
	                    },
	                    failed: function(properties) {
							console.log("The search has failed")
						},
	                    error: function(err) {
							console.log("The error is")
	                    }
				                });	            
	       });	


});	





$(document).on("click", "#g_refresh", function(event){
    	repos_dropdown_search.startSearch()
    	repos_table.startSearch();
    	latest_versions_search.startSearch();

    });


  });


